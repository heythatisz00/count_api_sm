from django.urls import path
from count import views

urlpatterns = [
    path('', views.main_page, name='main_page')
]
