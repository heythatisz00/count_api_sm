#!/bin/bash
echo "Creating virtual environment"
python -m venv venv
source ./venv/bin/activate
echo "installing the dependencies"
pip install -r requirements.txt
echo "Executing the webapp"
python manage.py runserver 0.0.0.0:3000