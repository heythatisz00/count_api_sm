#!/bin/bash
echo "Setting up the virtual environment"
source ./venv/bin/activate
echo "Executing the webapp"
python manage.py runserver 0.0.0.0:3000