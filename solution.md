# Solution 

In this project the user can hit the CountAPI and get/display the current count.
- It is assumed that your OS is Linux or Mac
- The Python version should be > 3.8
- It does not store the values in the database.


## Project Structure
The project structure is shown below.
It also briefs some of the project's important files.


```shell
├── count
│    ├── admin.py
│    ├── apps.py
│    ├── __init__.py
│    ├── migrations
│    ├── models.py
│    ├── templates          # The website templates are included here
│    │    ├── base.html     # The base html template which contains styling and required libraries
│    │    └── counter.html  # The html template for the counter api + the JQuery scripts
│    ├── tests.py
│    ├── urls.py            # The website's (sub) url/path for different pages are included here   
│    └── views.py           # The view and logic for different pages of the website
├── count_app
│    ├── settings.py
│    ├── urls.py            # All the website's url/path for different pages are included here
│    └── wsgi.py    
├── db.sqlite3              
├── manage.py               # Django management shell
├── solution.md             # this current file
├── static
│    ├── css                # CSS files
│    └── js                 # JavaScript files
```

## Execute webapp

### 1. Use Docker

1. Enter the root directory of the project
2. Build the image and the container (Optional)
```shell
docker-compose up -d
```
3. Run the webapp:

```shell
docker-compose run -d --service-ports count_app
```
*Note 1:* You may need to run the above commands using `sudo` command or as a superuser.
*Note 2:* To change the port, edit the `docker-compose.yml` file. And edit the Ports:
```yaml
ports:
    - "<host_port>:8000"
```
For example,
```yaml
ports:
    - "3000:8000"
```

### 2. Use The Provided Bash Script
##### For the first time
If you are executing the webapp for the first time run the bash script called `first_time_execute.sh`:
```shell
chmod +x first_time_execute.sh   # to make the file executable
```
```shell
./first_time_execute.sh         # execute the file
```

##### Run the app after the first time
To run the app for the subsequent times execute the `runserver.sh` file.

```shell
./runserver.sh    # first make the file executable: chmod +x runserver.sh
```

*Note:* To change the port number modify the `runserver.sh` and modify the line:
`python manage.py runserver 0.0.0.0:<new_port>` and insert the new port number.
For example, 
```shell
`python manage.py runserver 0.0.0.0:3000`
```

### 3. Using plain, vanilla, Python and Django

In the root directory of the webapp,

1. Install the required dependencies:
```shell
pip install -r requirements.txt
```
2. Run the Django app on port `3000`
```shell
$ python manage.py runserver 0.0.0.0:3000
```
*Note:* To change the port number modify the `runserver.sh` and modify the line:
`python manage.py runserver 0.0.0.0:<new_port>` and insert the new port number.
For example, 
```shell
`python manage.py runserver 0.0.0.0:3000`
```

## Configuring the webapp
To change the namespace and the key for the CountAPI, modify the `settings.py` file.
It is located in the `./count_app/settings.py`.

Locate the Count API setting section and modify the following variables as needed:
```python

COUNT_API_NAMESPACE = 'count_api_namespace'  # This should be unique
COUNT_API_KEY = 'count_API_key'
COUNT_API_GET_URL = "the_url_to_the_get_endpoint"
COUNT_API_HIT_URL = "the_url_to_the_hit_endpoint"
```


## Future updates

To:
- Provide a fancy graphic using css.
- Maybe store the counts (history) in the database.